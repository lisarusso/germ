import bz2
import heapq
import re
import tempfile
import os
import lxml.etree as ET
from xml.sax.saxutils import unescape

# TODO: Implement decompression for articleid
# TODO: Add fs compliance checks for output
# TODO: Fix block attack
# TODO: Guard against malicious xml
# TODO: Batch/regex on title article extraction
# TODO: Match output files with archive file name
# TODO: Refactor to ONE binary search, there's like half a dozen here
# TODO: Clean
# TODO: Docs

BLOCK_SIZE = 4
SORT_BUFFER_SIZE = 1000
ARTICLE_BLOCK_SIZE = 100
XML_ESCAPES = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>',
    '&quot;': '\\"',
    '&apos;': "\\'"
}


def decompress_block(fo, decompressor):
    decompressed = b''
    s_decompressed = ''
    prev_decompressed = b''

    while not decompressed and not decompressor.eof:
        block = fo.read(BLOCK_SIZE)
        decompressed = decompressor.decompress(block)

        if not decompressed: continue

        # The following is vulnerable to an attack (construct bz2 so that
        # each block has split utf-8 characters at end)
        decompressed = prev_decompressed + decompressed
        try:
            s_decompressed = decompressed.decode('utf-8')
            prev_decompressed = b''
        except UnicodeDecodeError as err:
            prev_decompressed = decompressed
            decompressed = b''
            continue
    return s_decompressed


def preprocess_index(archive, index):
    print('Preprocessing....')

    decompressor = bz2.BZ2Decompressor()

    archive_name = os.path.basename(archive.split('.')[0])

    with open(index, 'rb') as fo_index, \
            open(f'{archive_name}_aid.txt', 'wb') as fo_a, \
            open(f'{archive_name}_bytes.txt', 'wb') as fo_b, \
            open(f'{archive_name}_meta.txt', 'wb') as fo_m, \
            open(f'{archive_name}_root.txt', 'wb') as fo_r, \
            open(f'{archive_name}_titles.txt', 'wb') as fo_t:

        s_aid = ''
        s_strms = ''

        titles = []
        title = ''
        iter_titles = []
        strmbytes = 0
        prev_strmbytes = 0
        min_stream_byte = ""
        l_aid = 0
        u_aid = 0
        prev_decompressed = b''
        i = 0
        partial_line = ''
        last_line = ''
        longest_title = ''
        while not decompressor.eof:
            decompressed = decompress_block(fo_index, decompressor)
            lines = decompressed.split('\n')
            len_lines = len(lines) - 1
            for lno, l in enumerate(lines):
                if lno == 0:
                    if not re.match('\d+:\d+:\D+', l):
                        l = last_line + l
                    elif partial_line:
                        l = partial_line + l
                        partial_line = ''
                if lno == len_lines:
                    last_line = l
                    if not re.match('\d+:\d+:\D+', l):
                        partial_line = l
                        continue
                
                strmbytes, aid, title = l.split(':', 2)
                strmbytes, aid = int(strmbytes), int(aid)

                if strmbytes > prev_strmbytes:
                    s_aid += f':{l_aid},{u_aid},{prev_strmbytes}:\n'
                    prev_strmbytes = strmbytes
                    l_aid = aid
                    if min_stream_byte == "":
                        min_stream_byte = prev_strmbytes

                s_strms += f':{strmbytes},{title}:\n'
                
                titles.append(f':{title},{strmbytes}:')
                if len(title) > len(longest_title):
                    longest_title = title

                u_aid = aid

            if decompressor.eof:
                s_aid += f':{l_aid},{u_aid},{prev_strmbytes}:\n'
                titles.append(f':{title},{strmbytes}:')
                if strmbytes > prev_strmbytes:
                    s_strms += f':{strmbytes}:\n'

            if i % 50 == 0:
                print(f'Buffering... {i / 50}')
                titles.sort(key=lambda string: string.rsplit(',', 1)[0])
                s_titles = '\n'.join(titles) + '\n'
                fo_a.write(s_aid.encode('utf-8'))
                fo_b.write(s_strms.encode('utf-8'))
                tmp = tempfile.TemporaryFile(mode='w+b')
                tmp.write(s_titles.encode('utf-8'))
                tmp.seek(0)
                iter_titles.append(tmp)
                s_aid = ''
                s_strms = ''
                titles = []

            i += 1
            # Why does this not print true on the last thing?

        text = decompress_strm(archive, 0).decode('utf-8')
        root = text.split('\n', 1)[0]
        match = re.match('<(.*?)\s.*?>', root)
        root_tag = match.group(1)
        print(root_tag)
        text += f'</{root_tag}>'

        meta = f'max_aid:{u_aid}\n' \
               f'max_streambyte:{strmbytes}\n' \
               f'last_title:{title}\n' \
               f'longest_title:{longest_title}\n' \
               f'min_stream_byte:{min_stream_byte}'

        fo_a.write(s_aid.encode('utf-8'))
        fo_b.write(s_strms.encode('utf-8'))
        fo_m.write(meta.encode('utf-8'))
        fo_r.write(text.encode('utf-8'))

        s_titles = b''
        for t in heapq.merge(*iter_titles, key=lambda string: string.decode('utf-8').rsplit(',', 1)[0]):
            s_titles += t
            if len(s_titles) > SORT_BUFFER_SIZE:
                fo_t.write(s_titles)
                s_titles = b''
        if len(s_titles) > SORT_BUFFER_SIZE:
            fo_t.write(s_titles)


def decompress_strm(archive, start_bytes):
    decompressor = bz2.BZ2Decompressor()

    text = b''
    with open(archive, 'rb') as fo_a:
        fo_a.seek(start_bytes)
        while not decompressor.eof:
            text += decompress_block(fo_a, decompressor).encode('utf-8')

    return text


def find_start_byte(archive, index, strmbyte, max_strmbyte):
    start_byte = 0
    MAX_BYTE_ARCHIVE = os.path.getsize(archive)
    MAX_BYTE_INDEX = os.path.getsize(index)

    if max_strmbyte < strmbyte:
        return max_strmbyte

    with open(index, 'rb+') as fo_index:
        curr_strmbyte = 0
        l_block = 0
        u_block = MAX_BYTE_INDEX
        curr_block = (l_block + u_block) // 2
        seek = True
        found_start = False
        prev_byte = 0
        while not found_start:
            print(curr_strmbyte)
            if seek:
                # Chance of running into interrupted byte with this method? Need to account
                fo_index.seek(curr_block)
            lines = fo_index.read(len(str(MAX_BYTE_ARCHIVE) * 5)).decode('utf-8').split('\n')
            lines = lines[1:-1]
           
            lower_byte = int(lines[0].strip(':'))
            upper_byte = int(lines[-1].strip(':'))
            print(curr_block, lines, lower_byte, upper_byte)
            
            if lower_byte <= strmbyte <= upper_byte:
                for l in lines:
                    cand_byte = int(l.strip(':'))
                    if strmbyte == cand_byte:
                        start_byte = cand_byte
                        found_start = True
                        break
                    elif strmbyte > cand_byte:
                        prev_byte = cand_byte
                        continue
                    elif strmbyte < cand_byte:
                        start_byte = prev_byte
                        found_start = True
                        break
            elif strmbyte > upper_byte:
                l_block = curr_block
                curr_block = (curr_block + u_block) // 2
            elif strmbyte < lower_byte:
                u_block = curr_block
                curr_block = (curr_block + l_block) // 2

    return start_byte


def add_header(header, fo):
    fo.seek(0)
    fo.write(header[0])
    fo.seek(0, 2)
    fo.write(header[1])


def split_xml(fn, header, max_size):
    i = 1
    chunk = b''
    total_bytes = 0
    for _, page in ET.iterparse(fn, tag='{*}page'):
        if 'page' not in page.tag:  # be better!
            print('PAGE NOT IN TAG')
            continue
        text = ET.tostring(page, encoding='utf-8')
        if (total_bytes := len(text) + total_bytes) > max_size:
            print('WRITING')
            with open('templates/' + fn + str(i).zfill(5) + '.xml', 'wb') as fo_i:
                fo_i.write(header[0])
                fo_i.write(chunk)
                fo_i.write(header[1])
            total_bytes = 0
            i += 1
            chunk = b''
        chunk += text
        total_bytes += len(text)
        page.clear()
        print(f'total_bytes: {total_bytes}', end='\r')


def get_article_from_title(archive, index, meta, header, title, upper_title=None):
    articles = []
    streams_titles = {}
    start_bytes = set()
    MAX_BYTE_INDEX = os.path.getsize(index)
    LAST_TITLE = None  # Get last title
    LONGEST_TITLE = None # Get longest title

    if upper_title and upper_title < title:
        raise ValueError('upper_title cannot be less than than title')
    elif upper_title is None:
        upper_title = title

    with open(meta, 'r') as fo_meta:
        lines = fo_meta.readlines()
        lines = [l.strip().split(':', 1) for l in lines]
        lines = {field: val for (field, val) in lines}
        LAST_TITLE = lines['last_title']
        LONGEST_TITLE = lines['longest_title']

    with open(index, 'rb') as fo_index:
        l_title = ''
        u_title = LAST_TITLE
        l_block = 0
        u_block = MAX_BYTE_INDEX
        pl_title = ""
        pu_title = ""
        curr_block = (l_block + u_block) // 2
        seek = True
        found_start = False
        lines = []
        last_line = ''
        cand_title = ''
        cand_byte = 0

        # can factor a lot of this out
        # it's just a binary search, no reason we can't generalise to a new func

        while not found_start:
            if seek:
                fo_index.seek(curr_block)
            try:
                lines = fo_index.read(len(str(LONGEST_TITLE) * 5)).decode('utf-8').split('\n')
            except UnicodeDecodeError:
                curr_block +=1
                seek = True
                continue

            last_line = lines[-1]
            lines = lines[1:-1]
            l_title = lines[0][1:-1].rsplit(',', 1)[0]
            u_title = lines[-1][1:-1].rsplit(',', 1)[0]

            print(l_title, title, u_title)
            if pu_title == u_title and pl_title == l_title:
                return -1
            else:
                pu_title = u_title
                pl_title = l_title
            if l_title <= title <= u_title:
                for l in lines:
                    cand_title, cand_byte = l.strip(':').rsplit(',', 1)
                    cand_byte = int(cand_byte)
                    if cand_title == title:
                        start_bytes.add(cand_byte)
                        streams_titles[cand_byte] = [cand_title]
                        found_start = True
                        break
            elif title > u_title:
                l_block = curr_block
                curr_block = (curr_block + u_block) // 2
            elif title < l_title:
                u_block = curr_block
                curr_block = (curr_block + l_block) // 2

        i = 0
        pad = 0
        last_last_line = ''
        last_lines = [1]
        last_raw = b''
        while cand_title < upper_title:
            for l in lines:
                cand_title, cand_byte = l.strip(':').rsplit(',', 1)
                if cand_title > upper_title:
                    break
                if cand_title < title:
                    continue
                cand_byte = int(cand_byte)
                start_bytes.add(cand_byte)
                try:
                    streams_titles[cand_byte].append(cand_title)
                except KeyError:
                    streams_titles[cand_byte] = [cand_title]
            try:
                raw = last_raw + fo_index.read(len(str(LONGEST_TITLE) * 5) + pad)
                lines = raw.decode('utf-8').split('\n')
                last_raw = b''
                i = 0
            except UnicodeDecodeError:
                last_raw += raw
                continue
            i += 1

            last_last_line = last_line
            lines[0] = last_line + lines[0]
            last_line = lines[-1]
            lines = lines[:-1]

    head = []
    with open(header, 'rb') as fo_h:
        head = fo_h.read()
        root_end_tag = re.search(b'(</.*?>).*?$', head).group(1)
        head = [head.split(root_end_tag)[0], root_end_tag]

    ns = {}
    import pprint
    pprint.pprint(streams_titles)
    for start_byte, titles in streams_titles.items():
        text = head[0] + decompress_strm(archive, start_byte) + head[1]
        root = ET.fromstring(text)
        root_ns = re.match('{(.*?)}', root.tag).group(1)
        ns = {'ns': root_ns}
        for title in titles:
            title = unescape(title, XML_ESCAPES)
            try:
                articles.append(ET.tostring(root.xpath(f'ns:page/ns:title[.="{title}"]/..', namespaces=ns)[0], encoding='utf-8'))
            except ET.XPathEvalError:
                print('SKIPPED: ', title)
            except IndexError:
                print('skipped (index error): ', title)
        if len(articles) > ARTICLE_BLOCK_SIZE:
            pprint.pprint(streams_titles[start_byte])
            yield articles
            articles = []

    yield articles


def get_articles_from_stream(archive, start_byte, titles, head):
    """ Extract all articles from titles from stream beginning at start_byte """
    articles = []
    text = head[0] + decompress_strm(archive, start_byte) + head[1]
    root = ET.fromstring(text)
    root_ns = re.match('{(.*?)}', root.tag).group(1)
    ns = {'ns': root_ns}
    for title in titles:
        title = unescape(title, XML_ESCAPES)
        try:
            articles.append((title, ET.tostring(root.xpath(f'ns:page/ns:title[.="{title}"]/..', namespaces=ns)[0], encoding='utf-8')))
        except ET.XPathEvalError:
            print('SKIPPED: ', title)
        except IndexError:
            print('skipped (index error): ', title)
    return articles


def create_header(header):
    head = []
    with open(header, 'rb') as fo_h:
        head = fo_h.read()
        root_end_tag = re.search(b'(</.*?>).*?$', head).group(1)
        head = [head.split(root_end_tag)[0], root_end_tag]
    return head


def get_streambyte_from_title(index, title, meta):
    articles = []
    streams_titles = {}
    start_bytes = set()
    MAX_BYTE_INDEX = os.path.getsize(index)
    LAST_TITLE = None  # Get last title
    LONGEST_TITLE = None # Get longest title

    with open(meta, 'r') as fo_meta:
        lines = fo_meta.readlines()
        lines = [l.strip().split(':', 1) for l in lines]
        lines = {field: val for (field, val) in lines}
        LAST_TITLE = lines['last_title']
        LONGEST_TITLE = lines['longest_title']

    with open(index, 'rb') as fo_index:
        l_title = ''
        u_title = LAST_TITLE
        l_block = 0
        u_block = MAX_BYTE_INDEX
        pl_title = ""
        pu_title = ""
        curr_block = (l_block + u_block) // 2
        seek = True
        found_start = False
        lines = []
        last_line = ''
        cand_title = ''
        cand_byte = 0

        # can factor a lot of this out
        # it's just a binary search, no reason we can't generalise to a new func

        while not found_start:
            if seek:
                fo_index.seek(curr_block)
            try:
                lines = fo_index.read(len(str(LONGEST_TITLE) * 5)).decode('utf-8').split('\n')
            except UnicodeDecodeError:
                curr_block +=1
                seek = True
                continue

            last_line = lines[-1]
            lines = lines[1:-1]
            l_title = lines[0][1:-1].rsplit(',', 1)[0]
            u_title = lines[-1][1:-1].rsplit(',', 1)[0]

            print(l_title, title, u_title)
            if pu_title == u_title and pl_title == l_title:
                return -1
            else:
                pu_title = u_title
                pl_title = l_title
            if l_title <= title <= u_title:
                for l in lines:
                    cand_title, cand_byte = l.strip(':').rsplit(',', 1)
                    cand_byte = int(cand_byte)
                    if cand_title == title:
                        start_bytes.add(cand_byte)
                        streams_titles[cand_byte] = [cand_title]
                        found_start = True
                        break
            elif title > u_title:
                l_block = curr_block
                curr_block = (curr_block + u_block) // 2
            elif title < l_title:
                u_block = curr_block
                curr_block = (curr_block + l_block) // 2

        i = 0
        pad = 0
        last_last_line = ''
        last_lines = [1]
        last_raw = b''
        while cand_title <= title:
            for l in lines:
                cand_title, cand_byte = l.strip(':').rsplit(',', 1)
                if cand_title > title:
                    break
                if cand_title < title:
                    continue
                cand_byte = int(cand_byte)
                start_byte = int(cand_byte)
                start_bytes.add(cand_byte)
                try:
                    streams_titles[cand_byte].append(cand_title)
                except KeyError:
                    streams_titles[cand_byte] = [cand_title]
            try:
                raw = last_raw + fo_index.read(len(str(LONGEST_TITLE) * 5) + pad)
                lines = raw.decode('utf-8').split('\n')
                last_raw = b''
                i = 0
            except UnicodeDecodeError:
                last_raw += raw
                continue
            i += 1

            last_last_line = last_line
            lines[0] = last_line + lines[0]
            last_line = lines[-1]
            lines = lines[:-1]

    return start_byte


def get_titles_from_streambyte(index, stream_byte, meta):
    titles = []
    streams_titles = {}
    start_bytes = set()
    MAX_BYTE_INDEX = os.path.getsize(index)
    MAX_STREAM_BYTE = None  # Get last title
    LONGEST_TITLE = None # Get longest title
    MIN_STREAM_BYTE = 0

    with open(meta, 'r') as fo_meta:
        lines = fo_meta.readlines()
        lines = [l.strip().split(':', 1) for l in lines]
        lines = {field: val for (field, val) in lines}
        MAX_STREAM_BYTE = int(lines['max_streambyte'])
        LONGEST_TITLE = lines['longest_title']
        MIN_STREAM_BYTE = lines ['min_stream_byte']

    with open(index, 'rb') as fo_index:
        l_byte = 0
        u_byte = MAX_STREAM_BYTE
        l_block = 0
        u_block = MAX_BYTE_INDEX
        pl_byte = 0
        pu_byte = MAX_STREAM_BYTE
        curr_block = (l_block + u_block) // 2
        seek = True
        found_start = False
        lines = []
        last_line = ''
        cand_title = ''
        cand_byte = 0

        # can factor a lot of this out
        # it's just a binary search, no reason we can't generalise to a new func
        if stream_byte != MIN_STREAM_BYTE:
            while not found_start:
                if seek:
                    fo_index.seek(curr_block)
                try:
                    lines = fo_index.read(len(str(LONGEST_TITLE) * 5)).decode('utf-8').split('\n')
                except UnicodeDecodeError:
                    curr_block +=1
                    seek = True
                    continue

                last_line = lines[-1]
                lines = lines[1:-1]
                l_byte = int(lines[0][1:-1].split(',', 1)[0])
                u_byte = int(lines[-1][1:-1].split(',', 1)[0])

                print(curr_block, l_byte, stream_byte, u_byte)
                if u_block - l_block < 4:
                    return -1
                else:
                    pu_byte = u_byte
                    pl_byte = l_byte
                if l_byte < stream_byte <= u_byte:
                    for l in lines:
                        cand_byte, cand_title = l.strip(':').split(',', 1)
                        cand_byte = int(cand_byte)
                        if cand_byte == stream_byte:
                            start_bytes.add(cand_byte)
                            streams_titles[cand_byte] = [cand_title]
                            found_start = True
                elif stream_byte > u_byte:
                    l_block = curr_block
                    curr_block = (curr_block + u_block) // 2
                elif stream_byte <= l_byte:
                    u_block = curr_block
                    curr_block = (curr_block + l_block) // 2

        fo_index.seek(curr_block)
        fo_index.readline()
        while cand_byte <= stream_byte:
            l = fo_index.readline().strip()
            cand_byte, cand_title = l.strip(b':').split(b',', 1)
            cand_byte = int(cand_byte)
            if cand_byte < stream_byte:
                continue
            elif cand_byte > stream_byte:
                break
            titles.append(cand_title.decode('utf-8'))

        return titles
